#!/usr/bin/python3

from datetime import datetime, timedelta
from pprint import pprint


def parse_log(log_file_path):
    """Parse a uptimetracker log file and returns a list of events.

    Return a list of tuples like (event_datetime, event_name) in the order the events
    appear in the log file."""
    events = []
    file = open(log_file_path)
    cur_line = file.readline()
    while cur_line != '':
        cur_line = cur_line.strip("\n")
        datetime_str, event_name = cur_line.split(" ")
        date_str, time_str = datetime_str.split("T")
        [year, month, day] = date_str.split("-")
        year, month, day = int(year), int(month), int(day)
        [hour, minute, sec] = time_str.split(":")
        hour, minute, sec = int(hour), int(minute), int(sec)

        event_datetime = datetime(year, month, day, hour, minute, sec)
        events.append((event_datetime, event_name))

        cur_line = file.readline()
    return events


DOUBLE_UP_EVENT_ERROR_MSG = "ERROR: logs indicate %s event at %s while being up already."
DOUBLE_DOWN_EVENT_ERROR_MSG = "ERROR: logs indicate %s event at %s while being down already."
UNKNOWN_EVENT_ERROR_MSG = "ERROR: Unknown event %s while calculating stats."
LAST_EVENT_IS_UP_WARN_MSG = "WARNING: the last recorded event (%s) indicates that the system was UP."


def get_intervals(events):
    """Get a list of intervals (datetime_start, datetime_end)

    This function assumes that events are ordered by date."""
    intervals = []

    cur_is_up = False
    last_up = None
    for (event_datetime, event_name) in events:
        if event_name == "boot" or event_name == "wake":
            if cur_is_up:
                raise (BaseException(DOUBLE_UP_EVENT_ERROR_MSG %
                                     (event_name, event_datetime)))
            else:
                last_up = event_datetime
                cur_is_up = True
        elif event_name == "shutdown" or event_name == "sleep":
            if not cur_is_up:
                raise (BaseException(DOUBLE_DOWN_EVENT_ERROR_MSG %
                                     (event_name, event_datetime)))
            else:
                intervals.append((last_up, event_datetime))
                cur_is_up = False
        else:
            raise (BaseException(UNKNOWN_EVENT_ERROR_MSG % event_name))
    if cur_is_up:
        print(LAST_EVENT_IS_UP_WARN_MSG % last_up)
        print("Do you want to consider that the end of this interval is now ? y/n")
        if get_yes_no_input():
            intervals.append((last_up, datetime.now()))

    return intervals


def get_yes_no_input():
    while True:
        res = input()
        if res == "y":
            return True
        elif res == "n":
            return False
        else:
            print("Unrecognized input. Please try again.")


def get_daily_stat(intervals):
    daily_stat = {}

    for (datetime_start, datetime_end) in intervals:
        if datetime_start.date() == datetime_end.date():
            add_to_daily_stat(
                daily_stat,
                datetime_start.date(),
                datetime_end - datetime_start
            )
        else:
            print(
                "Logs indicate that the system was up past midnight. Splitting intervals is not supported yet. "
                "Interval will be counted as part the start date."
            )
            add_to_daily_stat(
                daily_stat,
                datetime_start.date(),
                datetime_end - datetime_start
            )

    return daily_stat


def add_to_daily_stat(daily_stat, date, duration):
    date_str = date.isoformat()

    if daily_stat.get(date_str) is None:
        daily_stat[date_str] = duration
    else:
        daily_stat[date_str] += duration


def timedelta_to_str(td):
    res = ""

    minutes = td.seconds // 60
    seconds = td.seconds % 60
    hours = minutes // 60
    minutes = minutes % 60

    res += str(hours + td.days * 24) + " hours, "
    res += str(minutes) + " minutes, "
    res += str(seconds) + " seconds."

    return res


def print_daily_stat(daily_stat):
    for day, td in daily_stat.items():
        print(day + ": " + str(td))


def sum_stats(stats):
    sum = timedelta(0)
    for td in stats.values():
        sum += td
    return sum


if __name__ == '__main__':
    my_events = parse_log("/var/log/uptimetracker/uptimetracker.log")
    my_intervals = get_intervals(my_events)
    my_daily_stat = get_daily_stat(my_intervals)

    print("daily stat:")
    print_daily_stat(my_daily_stat)

    print("sum:", timedelta_to_str(sum_stats(my_daily_stat)))
