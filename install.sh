#!/bin/bash
echo "Install script:"
if [ ! -d /opt/uptimetracker ]; then
	echo "Create /opt/uptimetracker directory"
	sudo mkdir /opt/uptimetracker
fi
sudo cp log_event.sh /opt/uptimetracker/

echo "Install hooks:"
echo "  Install sleep/wake hook:"
if [ ! -d /lib/systemd/system-sleep ]; then
	echo "    Create /lib/systemd/system-sleep directory"
	sudo mkdir /lib/systemd/system-sleep
fi 
echo "    Install uptimetracker_sleep_wake_hook.sh into /lib/systemd/system-sleep"
sudo cp ./hooks/uptimetracker_sleep_wake_hook.sh /lib/systemd/system-sleep

echo "  Install shutdown hook:"
echo "    Install uptimetracker_shutdown_hook.service into /etc/systemd/system/"
sudo cp ./hooks/uptimetracker_shutdown_hook.service /etc/systemd/system/

echo "  Install boot hook:"
echo "    Install uptimetracker_boot_hook.service into /etc/systemd/system/"
sudo cp ./hooks/uptimetracker_boot_hook.service /etc/systemd/system/

echo "  Systemctl daemon-reload"
sudo systemctl daemon-reload
echo "  Enable hook services"
sudo systemctl enable uptimetracker_shutdown_hook
sudo systemctl enable uptimetracker_boot_hook

echo "Install logrotate config"
if [ ! -d /var/log/uptimetracker ]; then
	echo "  Create /var/log/uptimetracker directory"
	sudo mkdir /var/log/uptimetracker
fi
sudo cp logrotate_config/uptimetracker /etc/logrotate.d

echo "Done."
