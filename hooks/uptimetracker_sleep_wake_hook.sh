#!/bin/bash
if [ $1 = "pre" ]; then
	if [ $2 = "suspend" ]; then
		/opt/uptimetracker/log_event.sh "sleep"
	fi
else
	if [ $1 = "post" ]; then
		if [ $2 = "suspend" ]; then
			/opt/uptimetracker/log_event.sh "wake"
		fi
	fi
fi
