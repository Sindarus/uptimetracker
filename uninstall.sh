#!/bin/bash
echo "Delete /opt/uptimetracker"
sudo rm -r /opt/uptimetracker

echo "Disable services:"
echo "  disable book hook"
sudo systemctl disable uptimetracker_boot_hook
echo "  disable shutdown hook"
sudo systemctl disable uptimetracker_shutdown_hook

echo "Systemctl daemon-reload"
sudo systemctl daemon-reload

echo "Delete hooks:"
echo "  Delete /lib/systemd/system-sleep/uptimetracker_sleep_wake_hook.sh"
sudo rm /lib/systemd/system-sleep/uptimetracker_sleep_wake_hook.sh

echo "  Delete /etc/systemd/system/uptimetracker_boot_hook.service"
sudo rm /etc/systemd/system/uptimetracker_boot_hook.service

echo "  Delete /etc/systemd/system/uptimetracker_shutdown_hook.service"
sudo rm /etc/systemd/system/uptimetracker_shutdown_hook.service

echo "Uninstall logrotate config"
sudo rm /etc/logrotate.d/uptimetracker

echo "Delete logs"
sudo rm -r /var/log/uptimetracker
