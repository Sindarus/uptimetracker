This is a solution to track the time when your computer is shut down, booted up, put in sleep, or woken up. This software will write its reports in a log file under `/var/log/uptimetracker.log`. Each time a boot/shutdown/sleep/wake event occurs, uptimetracker will log it along with the time of happening. Eg:
```
2018-06-22T22:24:08,461892911+02:00 boot
2018-06-22T22:25:16,734046746+02:00 sleep
2018-06-22T22:25:29,090846092+02:00 wake
2018-06-22T22:25:40,846143993+02:00 shutdown
```

#Installation instructions
Run install.sh
